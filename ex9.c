#include <stdio.h>

int main(int argc, char *argv[])
{
	int i = 0;

	while(i <= 24) {
		if(i % 2 == 0){	
			i++;
			continue;		
		}
		
		printf("%d \n", i);
		i++;
	}

	return 0;
}
