#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

struct Person {
	char *name;   //declares a pointer to a string (array of chars)
	int age;	//3 int declarations
	int height;
	int weight;
};

struct Person *Person_create(char *name, int age, int height, int weight) //Creates a "Person" struct with these parameters, returns a pointer to a Person struct
{
	struct Person *who = malloc(sizeof(struct Person));   //allocates enough memory for a Person struct and its 4 members
	assert(who != NULL);				      //ensures that the *who pointer is pointing to a valid memory address and does not return a NULL 									invalid pointer

	who->name = strdup(name);	//dereferences the Person struct and its name member variable to assign its name
	who->age = age;			//dereferences the Person struct and its age member variable to assign its age
	who->height = height;		//dereferences the Person struct and its height member variable to assign its height
	who->weight = weight;		//dereferences the Person struct and its weight member variable to assign its weight

	return who;			//returns the newly completed Person struct pointer
}

void Person_destroy(struct Person *who)
{
	assert(who != NULL);        //ensures the Person struct is pointing to a valid memory address

	free(who->name);	    //deallocates the memory previously created/given for the name string array member of the Person struct
	free(who);		      //deallocates the memory previously given/created for the Person struct itself
}

void Person_print(struct Person *who)
{
	printf("Name: %s\n", who->name); //print statements for each of the Person member variables
	printf("\tAge: %d\n", who->age);
	printf("\tHeight: %d\n", who->height);
	printf("\tWeight: %d\n", who->weight);
}

int main(int argc, char *argv[])
{
	//make two people structures
	struct Person *joe = Person_create("Joe Alex", 32, 64, 140);  
	struct Person *frank = Person_create("Frank Blank", 20, 72, 180);

	//print them out and where they are in memory; %p is for a pointer aka memory address for the start of the struct

	printf("Joe is at memory location %p:\n", joe);
	Person_print(joe);

	printf("Frank is at memory location %p:\n", frank);
	Person_print(frank);

	//make everyone age 20 years and print them again
	//note the use of the -> structure element pointer 
	//results in the value of the member of the structure as pointed to by the pointer

	joe->age += 20;
	joe->height += 2;
	joe->weight += 40;
	Person_print(NULL);

	frank->age += 20;
	frank->weight += 40;
	Person_print(NULL);

	//destroy them both so we clean up
	//passes in the struct pointers and deallocates the memory previously given for the Person name and then the Person struct itself

	Person_destroy(joe);
	Person_destroy(frank);

	return 0;
}
