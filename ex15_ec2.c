#include <stdio.h>

int main(int argc, char *argv[])
{
	int i = 0;

	//go through each string in argv, remember that *argv[0] is simply ./ex15_ec2 filename itself
	
	//creating a pointer to argv
	char **argv_ptr = argv;

	for(i = 0; i < argc; i++) {
		printf("arg %d: %s\n", i, argv_ptr[i]);	
	}

	printf("%p\n  %p\n %p\n %p\n", &argv_ptr[0], &argv_ptr[1], &argv_ptr[2], &argv_ptr[6]);	

	return 0;
}
