#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
     
#define ROWS 1000
#define COLUMNS 1000
#define SIDE ROWS
#define SEED_VALUE 2049
     
int matrix1[ROWS][COLUMNS];
int matrix2[ROWS][COLUMNS];
int resultMatrix[ROWS][COLUMNS];

void printMatrices();
void createMatrices();
void matrixMultiply();

void matrixMultiply() {
	int i,j,k;
	
	#pragma omp parallel for schedule(static, 50) private(i,j) shared(matrix1,matrix2,resultMatrix) collapse(2)
	for(i = 0; i < ROWS; i++){
		for(j = 0; j < COLUMNS; j++){
			for(k = 0; k < SIDE; k++) {
				resultMatrix[i][j] = matrix1[k][j] * matrix2[i][k];
			}
		}
	}
}
     
void printMatrices() {
     
	int i,j;
     
	for(i = 0; i < ROWS; i++) {
 		for(j = 0; j < COLUMNS; j++) {
    		printf("%d\t", matrix1[i][j]);
    		}
    	}		

	for(i = 0; i < ROWS; i++) {
 		for(j = 0; j < COLUMNS; j++) {
    		printf("%d\t", matrix2[i][j]);
    		}
	}

    	printf("\n");

	for(i = 0; i < ROWS; i++) {
 		for(j = 0; j < COLUMNS; j++) {
    		printf("%d\t", resultMatrix[i][j]);
		}
    	}
    	printf("\n");
}
     
void createMatrices() {
	srand(SEED_VALUE);
    	int i,j;
     
    	for(i = 0; i < ROWS; i++){
    		for(j = 0; j < COLUMNS; j++){
    			matrix1[i][j] = rand() % 10;
    		}	
    	}

    	for(i = 0; i < ROWS; i++){
    		for(j = 0; j < COLUMNS; j++){
    			matrix2[i][j] = rand() % 10;
    		}	
    	}
    }
     
int main()
{

	omp_set_num_threads(4);
	//omp_set_dynamic(0);

	createMatrices();
	matrixMultiply();
	printf("result: \n");
	printMatrices();

} //main
