#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
     
#define ROWS 1000
#define COLUMNS 1000
#define SIDE ROWS
#define SEED_VALUE 2049
     
typedef int matrix[ROWS][COLUMNS];

void printMatrix(matrix *matrixArg);
void createMatrix(matrix *matrixArg);
void matrixMultiply(matrix *m1, matrix *m2, matrix *resultMatrix);
int rowByColumn(matrix *m1, matrix *m2, int row, int column);

void matrixMultiply(matrix *m1, matrix *m2, matrix *resultMatrix) {
	int i,j;

	for(i = 0; i < ROWS; i++){
		for(j = 0; j < COLUMNS; j++){
			(*resultMatrix)[i][j] = rowByColumn(m1, m2, i, j);
		}
	}
}

int rowByColumn(matrix *m1, matrix *m2, int row, int column) {
	int result = 0;
	int i,j,k;
	
	for(k = 0; k < SIDE; k++){
		result += (*m1)[row][k] * (*m2)[k][column];
	}
	return result;
}
     
void printMatrix(matrix *matrixArg) {
     
	int i,j;
     
	for(i = 0; i < ROWS; i++) {
 		for(j = 0; j < COLUMNS; j++) {
    		printf("%d\t", (*matrixArg)[i][j]);
    	}
    	printf("\n");
    	}		
}
     
void createMatrix(matrix *matrixArg) {
	srand(SEED_VALUE);
    	int i,j;
     
    	for(i = 0; i < ROWS; i++){
    		for(j = 0; j < COLUMNS; j++){
    			(*matrixArg)[i][j] = rand() % 10;
    		}	
    	}
    }
     
int main()
{
	matrix *matrixPointer1 = malloc(ROWS * COLUMNS * sizeof(int));
	matrix *matrixPointer2 = malloc(ROWS * COLUMNS * sizeof(int));
	matrix *resultMatrix = malloc(ROWS * COLUMNS * sizeof(int));

	createMatrix(matrixPointer1);
	createMatrix(matrixPointer2);
     
    	printMatrix(matrixPointer1);
    	printf("\n");
    	printf("\n");
    	printMatrix(matrixPointer2);
    	printf("\n");
    	printf("\n");
	matrixMultiply(matrixPointer1, matrixPointer2, resultMatrix);
	printf("result: \n");
	printMatrix(resultMatrix);
    	free(matrixPointer1);
    	free(matrixPointer2);
	free(resultMatrix);
}//main
