
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) 
{
	// Initialize the MPI environment
	MPI_Init(NULL, NULL);

	// Find out rank, size
	int myrank;
	int world_size;
	int i;
	int *items;
	int *bigList;
	int *bossData;
	items = malloc(sizeof(int) * world_size);

	
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	srand(time(NULL));

	/////////////////////////////////////////////	
	if(myrank == 0)
	{
		int H = rand() % 1000 + 1;
		int N = rand() % 25 + 1;
		bossData[0] = N;//num items
		bossData[1] = H;//range

    	MPI_Bcast(&bossData, 2, MPI_INT, 0, MPI_COMM_WORLD);

//	for(i = 0; i < (boss_data[0] * world_size); i++){
//		printf("%d\n", bigList[i]);		
//	}

	}
	else{
	MPI_Bcast(&bossData, 2, MPI_INT, 0, MPI_COMM_WORLD);

		printf("%d has data from boss N = %d and H = %d\n", myrank, bossData[0], bossData[1]);		

		for(i = 0; i < bossData[0]; i++){
			items[i] = rand() % bossData[1] + 1; 
		}
	}

	if(myrank == 0){
		bigList = malloc(sizeof(int) * bossData[0] * world_size);
		MPI_Gather(items, bossData[0], MPI_INT, bigList, bossData[0], MPI_INT, 0, MPI_COMM_WORLD );
	}
	else{
		MPI_Gather(items, bossData[0], MPI_INT, NULL, 0, NULL, 0, MPI_COMM_WORLD );
	}
	
	free(items);
	free(bigList);

	/////////////////////////////////////////////	
	MPI_Finalize();
	return 0;
}





