#include <stdio.h>

int main()
{
	omp_set_num_threads(5);	

	#pragma comp parallel
	{
		printf("hi\n");
	}
	return 0;
}
