#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#define ROWS 4
#define COLUMNS 5
#define SEED_VALUE 24
#define GENERATIONS 1

typedef int twoDArray[ROWS][COLUMNS];
twoDArray currentArray;
twoDArray nextArray;

void modifyNode(int tempSum, int rowID, int columnID);
void printCurrentArray(void);
void printNextArray(void);
void fillInnerArray(void);
void getNextGen(void);
void copyNextToCurrent(void);

void copyNextToCurrent(void){
	int i,j;

	for(i = 0; i < ROWS; i++){
		for(j = 0; j < COLUMNS; j++){
			currentArray[i][j] = nextArray[i][j];
		}
	}
}

void modifyNode(int tempSum, int rowID, int columnID){
	if(tempSum % 10 == 0){
		nextArray[rowID][columnID] = 0;
	}
	else if(tempSum < 50){
		nextArray[rowID][columnID] = currentArray[rowID][columnID] + 3;
	}
	else if(tempSum > 50 && tempSum < 150){
		if((currentArray[rowID][columnID] - 3) >= 0){		
			nextArray[rowID][columnID] = currentArray[rowID][columnID] - 3;
		}
		else{
			nextArray[rowID][columnID] = 0;
		}
	}
	else{
		nextArray[rowID][columnID] = 1;
	}
}

void getNextGen(){
	int i,j;
	int tempSum = 0;

	for(i = 1; i < ROWS-1; i++){
		for(j = 1; j < COLUMNS-1; j++){
			tempSum += currentArray[i][j];//node value
			tempSum += currentArray[i-1][j-1];//upper-left
			tempSum += currentArray[i-1][j];//above
			tempSum += currentArray[i-1][j+1];//upper-right
			tempSum += currentArray[i][j+1];//right
			tempSum += currentArray[i+1][j+1];//lower-right
			tempSum += currentArray[i+1][j];//below
			tempSum += currentArray[i+1][j-1];//lower-left
			tempSum += currentArray[i][j-1];//left
			//printf("%d\n", tempSum);
			modifyNode(tempSum, i, j);
			tempSum = 0;
		}
	}
}

void printCurrentArray(){
	int i,j;
	
	for(i = 0; i < ROWS; i++){
		for(j = 0; j < COLUMNS; j++){
			printf("%d\t", currentArray[i][j]);
		}
		printf("\n");	
	}
}

void printNextArray(){
	int i,j;
	
	for(i = 0; i < ROWS; i++){
		for(j = 0; j < COLUMNS; j++){
			printf("%d\t", nextArray[i][j]);
		}
		printf("\n");	
	}
}

void fillInnerArray(void) { //only used once to generate the initial "current" array with random values;
	int i,j;	
	//srand();
	
	for(i = 1; i < ROWS-1; i++){
		for(j = 1; j < COLUMNS-1; j++){
			currentArray[i][j] = rand() % 20;
		}
	}
}

int main()
{
	int i,j;//fill arrays with zeros to start

	for(i = 0; i < ROWS; i++){
		for(j = 0; j < COLUMNS; j++){
			currentArray[i][j] = 0;
			nextArray[i][j] = 0;
		}	
	}

	fillInnerArray();//generate initial random array

	
	for(i = 0; i < GENERATIONS; i++){ //start modifying this next. 
		printCurrentArray();
		getNextGen();
		copyNextToCurrent();
		printf("\n");
		printf("\n");
		printNextArray();
		printf("\n");
		printf("\n");	
	}

}//main

