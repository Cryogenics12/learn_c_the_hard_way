#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>


int MAX = 10;
int **Matrix[3];
int size_m = 10;
int numTHREADS = 5;

int get_cell(int **Matrix[3], int r, int c, int size_m)
{
    int answer = 0;

    for (int k = 0; k < size_m; ++k)
    {
        //printf("%3d %3d  ", i , j );
        //printf("%d = %d + %d * %d\n", answer, answer, Matrix[0][i][k], Matrix[1][k][j]);
        answer = answer + Matrix[0][r][k]*Matrix[1][k][c]; 
              
    }
    //printf("%d\n", answer);
    //printf("\n");
                   
    return answer;
}

void Matrix_Multiply(int **Matrix[3], int size_m, int myid )
{
    int WL = ((size_m)*(size_m));
    int r, c;
   
   //Matrix[2][0][0] = get_cell(Matrix, 0, 0, 3);
    int i = myid;
    while ( i <= WL )
    {
        r = (int)i/size_m;
        c = i%size_m;
            if(c == 0){r--;} 

        Matrix[2][r][c] = get_cell(Matrix, r, c, size_m);

        i = i + numTHREADS;     
    }

}

void Print_Matrix(int **Matrix[3], int size_m)
{
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < size_m; ++j)
        {
            for (int k = 0; k < size_m; ++k)
            {
               printf("%4d", Matrix[i][j][k] );
            }
            printf("\n");
        }
        printf("\n");
        printf("\n");
    }
}

void *entry_point( void *arg)
{
    int myid = (long)arg;

    Matrix_Multiply(Matrix, size_m, myid);
}

int main(int argc, char **argv)
{
    void *status;
    pthread_t tid[numTHREADS+1]; //srand(time(NULL));
    clock_t start_t, end_t;

    //Malloc 3 Matrix 
    for (int i = 0; i < 3; ++i)
    {
        Matrix[i] = (int**)malloc( size_m * sizeof(int*) );
        for (int j = 0; j < size_m; ++j)
        {
                Matrix[i][j] = (int*)malloc( size_m*size_m*sizeof(int) );
        }
    }

    //Seed Matrix With Random Numbers
    for (int i = 0; i < 2; ++i)
        for (int j = 0; j < size_m; ++j)
            for (int k = 0; k < size_m; ++k)
               Matrix[i][j][k] = rand()%MAX+1; 

    //-----------------------------------------
    
    for (int i = 1; i <= numTHREADS; ++i)
    {
        pthread_create( &tid[i], NULL, entry_point, (void*)(long)i);  
    }

    for (int i = 1; i <= numTHREADS; ++i)
    {
        pthread_join( tid[i], &status );  
    }


    //-----------------------------------------

    Print_Matrix(Matrix, size_m);


    //Delete Array Matrix[][][]
    for (int i = 0; i < 3; ++i)
    {
       for (int j = 0; j < size_m; ++j)
       {
           free(Matrix[i][j]);
       }
       free(Matrix[i]);
    }

    return 0;
}








