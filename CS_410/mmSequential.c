#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>



int get_cell(int **Matrix[3], int i, int j, int size_m)
{
    int answer = 0;

    for (int k = 0; k < size_m; ++k)
    {
        //printf("%d = %d + %d * %d\n", answer, answer, Matrix[0][i][k], Matrix[1][k][j]);
        answer = answer + Matrix[0][i][k]*Matrix[1][k][j]; 
              
    }
    //printf("%d\n", answer);
    //printf("\n");
                   
    return answer;
}

void Matrix_Multiply(int **Matrix[3], int size_m)
{
   //Matrix[2][0][0] = get_cell(Matrix, 0, 0, 3);
    for (int i = 0; i < size_m; ++i)
    {
        for (int j = 0; j < size_m; ++j)
        {
            Matrix[2][i][j] = get_cell(Matrix, i, j, size_m);
        }
    }

}

void Print_Matrix(int **Matrix[3], int size_m)
{
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < size_m; ++j)
        {
            for (int k = 0; k < size_m; ++k)
            {
               printf("%4d", Matrix[i][j][k] );
            }
            printf("\n");
        }
        printf("\n");
        printf("\n");
    }


}

int main(int argc, char **argv)
{
    //srand(time(NULL));
    int MAX = 10;
    int **Matrix[3];
    int size_m = 10;

    //Malloc 3 Matrix 
    for (int i = 0; i < 3; ++i)
    {
        Matrix[i] = (int**)malloc( size_m * sizeof(int*) );
        for (int j = 0; j < size_m; ++j)
        {
                Matrix[i][j] = (int*)malloc( size_m*size_m*sizeof(int) );
        }
    }

    //Seed Matrix With Random Numbers
    for (int i = 0; i < 2; ++i)
        for (int j = 0; j < size_m; ++j)
            for (int k = 0; k < size_m; ++k)
               Matrix[i][j][k] = rand()%MAX+1; 

    Matrix_Multiply(Matrix, size_m);
    Print_Matrix(Matrix, size_m);

    for (int i = 0; i < 3; ++i)
    {
       for (int j = 0; j < size_m; ++j)
       {
           free(Matrix[i][j]);
       }
       free(Matrix[i]);
    }

    return 0;
}








