#include <stdio.h>

int main(int argc, char *argv[])
{
	char full_name[] = {
	'Z', 'e', 'd',
	' ', 'A', '.', ' ',
	'S', 'h', 'a', 'w'
	};	

	int areas[] = {10, 12, 13, 14, 20};
	char name[] = "Jon";
	int *ptr_to_areas = areas;
	char *ptr_to_name = name;

	areas[0]= 100;
	
	name[1] = 'P';
	full_name[8] = 'U';

	full_name[8] = name[1];

	//Warning: On some systems you may have to change the %ld in this code to a %u since it will use unsigned ints

	printf("The size of an int: %ld\n", sizeof(int));

	printf("The size of an int pointer: %ld\n", sizeof(ptr_to_areas[0]));

	printf("The size of areas (int[]): %ld\n", sizeof(areas));

	printf("The number of ints in areas: %ld\n", sizeof(areas) / sizeof(areas[0]));
	
	printf("The first area is %d, the 2nd area is %d.\n", ptr_to_areas[0], ptr_to_areas[1]);
	
	printf("The size of a char: %ld\n", sizeof(char));

	printf("The size of name (char[]): %ld\n", sizeof(name));

	printf("The number of chars: %ld\n", sizeof(name) / sizeof(ptr_to_name[0]));

	printf("The size of full_name (char[]): %ld\n", sizeof(full_name));

	printf("The number of chars: %ld\n", sizeof(full_name) / sizeof(char));

	printf("name=\"%c%c%c\" and full_name=\"%s\"\n", ptr_to_name[0], ptr_to_name[1], ptr_to_name[8], full_name);

	areas[0] = 177;

	
	return 0;

}//main
