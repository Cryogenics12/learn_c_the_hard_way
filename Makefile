CFLAGS=-Wall -g

all: ex1 ex3 ex7 ex8 ex9 ex10 ex10_ec ex11 ex11_ec ex12 ex13 ex14 sizeOfTest ex15 pointers ex15_ec ex15_ec2 ex15_ec3 ex16 ex16_ec ex17

clean:
	rm -f ex1
	rm -f ex3
	rm -f ex7
	rm -f ex8
	rm -f ex9
	rm -f ex10
	rm -f ex10_ec
	rm -f ex11
	rm -f ex11_ec
	rm -f ex12
	rm -f ex13
	rm -f ex14
	rm -f sizeOfTest
	rm -f ex15
	rm -f pointers
	rm -f ex15_ec
	rm -f ex15_ec2
	rm -f ex15_ec3
	rm -f ex16
	rm -f ex16_ec
	rm -f ex17
