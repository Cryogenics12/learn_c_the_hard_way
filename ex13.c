#include <stdio.h>

int main(int argc, char *argv[])
{
	int i = 0;

	//go through each string in argv
	//why am I skipping argv[0]? Because that spot is for when I type ./ex13 as an argument to execute the program

	char *states[] = {
		"California", "Oregon",
		"Washington", "Texas",
		NULL
	};	

	int num_states = 5;

	argv[1] = states[1];
	states[2] = argv[2];

	for(i = 0 ; i < argc; i++) {
		printf("arg %d: %s\n", i, argv[i]);
	}

	//let's make our own array of strings





	for(i = 0; i < num_states; i++) {
		printf("state %d: %s\n", i, states[i]);
	}

	return 0;
}//main
