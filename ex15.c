#include <stdio.h>

int main(int argc, char *argv[]) 
{
	// create two arrays we care about
	int ages[] = {23, 43, 12, 89, 2};
	char *names[] = {
		"Alan", "Frank",
		"Mary", "John", "Lisa"	
	};

	//alternative yet annoying way of rewriting the *names[] pointer to just an array
	//char alt_names[] = {'A', 'l', 'a', 'n', ' ', 'F', 'r', 'a', 'n', 'k', ' ', 'M', 'a', 'r', 'y', ' ', 'J', 'o', 'h', 'n', ' ', 'L', 'i', 's', 'a'};

	//safely get the size of ages
	int count = sizeof(ages) / sizeof(int);    //Stack overflow suggests writing this as sizeof(ages)/sizeof(ages[0])
	int i = 0;

	// first way using indexing 
	for(i = count - 1; i >= 0; i--) {
		printf("%s has %d years alive.\n", names[i], ages[i]);	
	}

	// treating the array as a pointer
	for(i = 0; i < count; i++) {
		printf("%s has lived %d years so far.\n", *(names + i), *(ages + i));	
	}

	printf("---\n");

	// set up the pointers to the start of the arrays
	int *cur_age = ages;
	char **cur_name = names; //pointer to a pointer that points to an array of character strings

	//second way using pointers
	for(i = count - 1; i >= 0; i--) {
		printf("%s is %d years old.\n", *(cur_name + i), *(cur_age + i)  );	
	}

	printf("---\n");

	// third way, pointers are just arrays
	for(i = count - 1; i >= 0; i--) {
		printf("%s is %d years old again.\n", cur_name[i], cur_age[i] );	
	}

	printf("---\n");

	// fourth way with pointers in a stupid complex way
	for(cur_name = names + 4, cur_age = ages + 4; (cur_age - ages) >= 0; cur_name--, cur_age--) {
		printf("%s lived %d years so far.\n", *cur_name, *cur_age);
		//printf("Value of cur_age pointer at this step: %p\n", cur_age);
	}

	printf("---\n");

	//printf("Value of cur_ages, a memory address, pointing to ages: %p\n", cur_age);
	//printf("A slightly different way of printing the memory address: %p\n", &ages);
	//printf("Size of pointer to int array: %ld\n", sizeof(cur_age));
	//printf("Size of actu al contents in int array: %ld\n", sizeof(ages));


	return 0;
}
