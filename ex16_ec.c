#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

struct Person {
	char *name;
	int age;
	int height;
	int weight;
};

struct Person Person_create(char *name, int age, int height, int weight) {

	struct Person who;

	who.name = strdup(name);
	who.age = age;
	who.height = height;
	who.weight = weight;

	return who;
} 

int main(int argc, char *argv[]){
	struct Person joe = {"Joe Frazier", 33, 84, 225};
	printf("The mighty %s\nAge: %d\nHeight: %d\nWeight: %d\n", joe.name, joe.age, joe.height, joe.weight);
	//because this Person struct is created on the stack and does not use malloc it should not have a corresponding free()
	return 0;
}
