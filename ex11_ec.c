#include <stdio.h>

int main(int argc, char *argv[])
{
    char name[4] = { 'a', 'b', 'c', 'd' };
    int number = (name[0] << 24) + (name[1] << 16) + (name[2] << 8) + name[3];

    printf("number: %d\n", number);

    return 0;
}
