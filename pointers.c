#include <stdio.h>

int main(int argc, char *argv[])
{
	int ages[] = {23, 43, 12, 89, 2};

	int *cur_age = ages;

	printf("Value of cur_ages, a memory address, pointing to ages: %p\n",    cur_age);
	printf("A slightly different way of printing the memory address: %p\n", &ages);

	printf("The cur_age value incremented once: %p\n", cur_age + 1);
	printf("The cur_age value incremented twice: %p\n", cur_age + 2);
	printf("The cur_age value incremented three times: %p\n", cur_age + 3);
	printf("The cur_age value incremented four times: %p\n", cur_age + 4);
	printf("The size of an int in bytes: %ld\n", sizeof(int));

	return 0;
}
