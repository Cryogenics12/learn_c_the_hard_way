#include <stdio.h>
#include <ctype.h>

void ptr_print1(int count, int ages[], char *names[]);
void ptr_print2(int count, int *cur_age, char **cur_name);
void ptr_print3(int count, int *cur_age, char **cur_name);
void ptr_print4(int count, int ages[], char *names[], int *cur_age, char **cur_name);

void ptr_print1(int count, int ages[], char *names[]) 
{
	int i = 0;

	for(i = 0; i < count; i++) {
		printf("%s has %d years alive.\n", names[i], ages[i]);		
	}
}

void ptr_print2(int count, int *cur_age, char **cur_name)
{
	int i = 0;

	for(i = 0; i < count; i++) {
		printf("%s is %d years old.\n", *(cur_name + i), *(cur_age + i));	
	}
}

void ptr_print3(int count, int *cur_age, char **cur_name)
{
	int i = 0;

	for(i = 0; i < count; i++) {
		printf("%s is %d years old.\n", cur_name[i], cur_age[i]);
	}
}

void ptr_print4(int count, int ages[], char *names[], int *cur_age, char **cur_name)
{
	for(cur_name = names, cur_age = ages;
		(cur_age - ages) < count; cur_name++, cur_age++) {
		printf("%s lived %d years so far.\n", *cur_name, *cur_age);
	}
}

int main(int argc, char *argv[])
{
	int ages[] = {23, 43, 12, 89, 2};	
	char *names[] = {"Alan", "Frank", "Mary", "John", "Lisa"};

	int count = sizeof(ages) / sizeof(ages[0]);

	int *cur_age = ages;
	char **cur_name = names;

	ptr_print1(count, ages, names);
	ptr_print2(count, cur_age, cur_name);
	ptr_print3(count, cur_age, cur_name);
	ptr_print4(count, ages, names, cur_age, cur_name);

	return 0;
}
