#include <stdio.h>

char toLowerCase(char someLetter) {
	if(someLetter >= 97 && someLetter <= 122){ 
		return someLetter;
	}
	else if(someLetter >= 65 && someLetter <= 90){
		return someLetter + 32;
	}
	else{
		return someLetter;
	}
}//toLowerCase

int main(int argc, char *argv[])
{
	if(argc < 2) {
		printf("ERROR: You need at least one argument.\n");
		//this is how you abort a program
		return 1;	
	}
	
	int i = 0;
	int j = 1;

	for(j = 1; argv[j] != '\0'; j++){
	
		for(i = 0; argv[j][i] != '\0'; i++){

			char letter = argv[j][i];
			char lowerCaseLetter = toLowerCase(letter);		
		
			switch(lowerCaseLetter) {
				case 'a':
					printf("%d: 'A'\n", i);
					break;

				case 'e':
					printf("%d: 'E'\n", i);
					break;

				case 'i':
					printf("%d: 'I'\n", i);
					break;	

				case 'o':
					printf("%d: 'O'\n", i);
					break;

				case 'u':
					printf("%d: U'\n", i);
					break;

				case 'y':
					if(i > 2) {
						//it's only sometimes Y
						printf("%d: 'Y'\n", i);
						break;
					}

				default:
					printf("%d: %c is not a vowel\n", i, lowerCaseLetter);

			}//switch

	
		}//inner for-loop
	
	}//outer for-loop
	return 0;
}//main
